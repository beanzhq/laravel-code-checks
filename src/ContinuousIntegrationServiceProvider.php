<?php

namespace Beanz\CodeChecks;

use Illuminate\Support\ServiceProvider;

class ContinuousIntegrationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/circleci/.circleci.env' => base_path('.circleci/.circleci.env'),
            __DIR__ . '/circleci/config.yml' => base_path('.circleci/config.yml'),
        ]);
    }
}
