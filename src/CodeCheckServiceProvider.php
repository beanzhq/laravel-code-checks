<?php

namespace Beanz\CodeChecks;

use Illuminate\Support\ServiceProvider;

class CodeCheckServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/grump-config/.php_cs' => base_path('grump-config/.php_cs'),
            __DIR__ . '/grump-config/grumphp.yml' => base_path('grump-config/grumphp.yml'),
            __DIR__ . '/grump-config/phpcs.xml' => base_path('grump-config/phpcs.xml'),
            __DIR__ . '/grump-config/ruleset.xml' => base_path('grump-config/ruleset.xml'),
        ]);
    }
}
